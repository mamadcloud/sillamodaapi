class Product < ActiveRecord::Base
  self.table_name = 'Product'

  def self.all_active
    where(status: 1)
  end

  def self.find_active id
    product = where(id: id, status: 1).first!
    if product.nil?
        raise ActiveRecord::RecordNotFound
    end
    product 
  end 
end
