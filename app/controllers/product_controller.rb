class ProductController < ApplicationController
  before_filter :ensure_json_request 

  def index
     products = Product.all_active
     render json: products
  end

  def get
     product = Product.find_active(params[:id])
     render json: product
  end

  def add
    product = Product.new(product_params)
    product.save
    render json: product
  end

  def update
    product = Product.find(params[:id])

    if product.update_attributes(product_params)
      render json: product
    else
      raise ActiveRecord::RecordNotFound
    end 
  end

  def delete
    product = Product.find(params[:id])
    if product.delete()
      render json: "{ \"status\" : 1 }"
    else 
      raise ActiveRecord::RecordNotFound
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.permit(:subcategoryid, :name, :detail, :ingredient, :howtouse, :volume, :status, :price, :discountprice)
    end

    def ensure_json_request
       unless request.headers["Content-Type"].eql? "application/json"
          render json: "{ \"message\" : \"Only accept JSON format\" }", :status => 406
       end
    end
end
