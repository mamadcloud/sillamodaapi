class ApplicationController < ActionController::API
#class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception
  
  unless Rails.application.config.consider_all_requests_local
    rescue_from Exception do |exception|

      # Put loggers here, if desired.

      app_error 500
    end
    rescue_from ActionController::RoutingError, 
		ActionController::UnknownController, 
		::AbstractController::ActionNotFound, 
		ActiveRecord::RecordNotFound do |exception|
      #not_found
      app_error 404
    end
  end

  #def not_found
    #raise ActionController::RoutingError.new('Not Found')
    #yield
    #rescue ActionController::RoutingError, ActionController::UnknownController, ::AbstractController::ActionNotFound, ActiveRecord::RecordNotFound 
  #  redirect_to '/404.html', :flash => { :error => "Record not found." }
  #end

  #def app_error
  #  redirect_to '/500.html'
  #end 

  def app_error status
    #render plain: '#{status}'
    #render json: "{ \"message\" : #{status} }"
    redirect_to "/error/error_#{status}"
  end
end
