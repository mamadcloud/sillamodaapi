class ErrorController < ApplicationController
	def error_404
          render json: '{ "status" : 404 }'
	end
	
	def error_500
          render json: '{ "status" : 500 }'
	end
end
