class CategoryController < ApplicationController
  before_filter :ensure_json_request 

  def index
    categories = Category.all

    render json: categories
  end

  def get
    category = Category.find(params[:id])

    render json: category
  end

  def add
    category = Category.new(category_params)

    category.save
    render json: category
  end 

  def update
    category = Category.find(params[:id])
    
    if category.update_attributes(category_params)
      render json: category
    else
      raise ActiveRecord::RecordNotFound
    end
  end

  def delete
    category = Category.find(params[:id])

    if category.delete
      render json: "{ \"status\" : 1 }"
    else
      raise ActiveRecord::RecordNotFound
    end
  end

  private
    def category_params
      params.permit(:name)
    end

    def ensure_json_request
       unless request.headers["Content-Type"].eql? "application/json"
          render json: "{ \"message\" : \"Only accept JSON format\" }", :status => 406
       end
    end
end
