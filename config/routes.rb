Rails.application.routes.draw do
  #get 'helper/index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  
  scope '/v1' do
    scope '/products' do
      get '/' => 'product#index'
      post '/' => 'product#add'
      scope '/:id' do
        get '/' => 'product#get'
        put '/' => 'product#update'
        delete '/' => 'product#delete'
        scope '/images' do
          get '/' => 'image#index'
         # post '/' => 'product#addimages'
         # scope '/:image-id' do 
         #   get '/' => 'product#getimage'
         #   delete '/' => 'product#deleteimage'
         # end
        end
      end
    end
    scope '/images' do 
       post '/' => 'image#add'
       scope '/:id' do
         get '/' => 'image#get'
         delete '/' => 'image#delete'
       end
    end
    scope '/categories' do
       get '/' => 'category#index'
       post '/' => 'category#add'
       scope '/:id' do
         get '/' => 'category#get'
         put '/' => 'category#update'
         delete '/' => 'category#delete'
         scope '/subcategories' do
           get '/' => 'subcategory#index'
           #post '/' => 'subcategory#add'
           #scope '/:subcategory-id' do
           #  get '/' => 'subcategory#get'
           #  put '/' => 'subcategory#update'
           #  delete '/' => 'subcategory#delete'
           #end
         end
       end
    end
    scope '/subcategory' do
      post '/' => 'ubcategory#add'
      scope '/:id' do
        get '/' => 'subcategory#get'
        put '/' => 'subcategory#update'
        delete '/' => 'subcategory#delete'
      end
    end
  end
  scope '/helper' do
    get '/' => 'helper#index'
    get '/:name' => 'helper#get'
  end
  get '/', to: redirect('/helper')
  get 'error/error_404'
  get 'error/error_500'
  get '*path' => redirect('/error/error_404')
end
