class CreateRelatedProducts < ActiveRecord::Migration
  def change
    create_table :related_products do |t|

      t.timestamps null: false
    end
  end
end
