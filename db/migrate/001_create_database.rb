class CreateDatabase < ActiveRecord::Migration
  def up
    	  create_table "Category", force: :cascade do |t|
	    t.string "name", limit: 255, null: false
	  end

	  create_table "Product", force: :cascade do |t|
	    t.integer "subcategoryid", limit: 4,                              null: false
	    t.string  "name",          limit: 255,                            null: false
	    t.text    "detail",        limit: 65535,                          null: false
	    t.text    "ingredient",    limit: 65535
	    t.text    "howtouse",      limit: 65535
	    t.string  "volume",        limit: 300
	    t.binary  "status",        limit: 1
	    t.decimal "price",                       precision: 15, scale: 2
	    t.decimal "discountprice",               precision: 15, scale: 2
	  end

	  create_table "ProductImage", force: :cascade do |t|
	    t.integer "productid", limit: 8,   null: false
	    t.string  "caption",   limit: 255
	    t.binary  "isdefault", limit: 1
	  end

	  create_table "RelatedProduct", force: :cascade do |t|
	    t.integer "productid",        limit: 8
	    t.integer "relatedproductid", limit: 8
	  end

	  create_table "Subcategory", force: :cascade do |t|
	    t.integer "categoryid", limit: 4,   null: false
	    t.string  "name",       limit: 255, null: false
	  end
  end

  def down
    # drop all the tables if you really need
    # to support migration back to version 0
  end
end
